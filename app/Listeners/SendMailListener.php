<?php

namespace App\Listeners;

use App\Events\SendMail;
use App\Mail\CreateProduct;
use App\Models\Product;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendMailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendMail  $event
     * @return void
     */
    public function handle(SendMail $event)
    {
        \Mail::to('waleed@gmail.com')->send(new CreateProduct(['data' => $event->product]));
    }
}
