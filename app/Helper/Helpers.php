<?php

# uploadFiles
if (!function_exists('uploadFiles'))
{
    function uploadFiles()
    {
        return new \App\Http\Controllers\Upload\UploadController();
    }

}

if (!function_exists('storeUpload'))
{
    function storeUpload($file, $path, $upload_type, $delete_file = null, $relation_id = null, $file_type =null, $multi_upload =null )
    {
        $data = uploadFiles()->upload([
            'file' => $file,
            'path' => $path,
            'upload_type' =>$upload_type,
            'file_type'=>$file_type,
            'delete_file'=> $delete_file,
            'relation_id'=> $relation_id,
            'multi_upload' => $multi_upload
        ]);
        return $data;
    }

}
