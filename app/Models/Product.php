<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = [
      'name','price','image','description'
    ];

    protected $appends =['imagePath'];

    public function getNameAttribute($value)
    {
        return ucfirst($value);
    }

    public function getImagePathAttribute()
    {
        return asset('storage/'.$this->image);
    }

    public function files()
    {
        return $this->hasMany(File::class,'relation_id','id')
            ->where('file_type','=','product');
    }
}
