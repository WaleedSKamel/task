<?php


namespace App\Repositories;


use App\Events\SendMail;
use App\Interfaces\ProductRepositoryInterface;
use App\Models\File;
use App\Models\Product;

class ProductRepository implements ProductRepositoryInterface
{
    public function all()
    {
        try {
            $products = Product::orderByDesc('created_at')->get();

            return $products;

        } catch (\Exception $exception) {
            return redirect()->back()->with(['error' => $exception->getMessage()]);
        }
    }

    public function store($request)
    {
         try {
             $data = $request->except('image','images');
             if ($request->hasFile('image')){
                 $data['image'] = storeUpload('image','product','single');
             }
             $product = Product::create($data);
             if ($product){
                 if ($request->hasFile('images') && $request->images){
                     storeUpload('images','product/'.$product->id,'files','',
                         $product->id,'product',true);
                 }
                 event(new SendMail($product));

                 return  redirect()->back()->with(['success' => 'Done save product']);
             }
             return  redirect()->back()->with(['error' => 'Please try again']);
        } catch (\Exception $exception) {
            return redirect()->back()->with(['error' => $exception->getMessage()]);
        }


    }

    public function edit($id)
    {
        try {
            $product = Product::with('files')->find($id);
            if ($product){
                return  view('backend.product.edit',compact('product'));
            }
            return redirect()->back()->with(['error' => 'This Product Not found']);
        } catch (\Exception $exception) {
            return redirect()->back()->with(['error' => $exception->getMessage()]);
        }
    }

    public function update($request ,$id)
    {
        try {
            $product = Product::find($id);

            $data = $request->except('image','images');
            if ($request->hasFile('image')){
                $data['image'] = storeUpload('image','product','single',$product->image);
            }
            $product->update($data);
            if ($product){
                if ($request->hasFile('images') && $request->images){
                    storeUpload('images','product/'.$product->id,'files','',
                        $product->id,'product',true);
                }
                return  redirect()->route('product.view')->with(['success' => 'Done update product']);
            }
            return  redirect()->back()->with(['error' => 'Please try again']);
        } catch (\Exception $exception) {
            return redirect()->back()->with(['error' => $exception->getMessage()]);
        }
    }

    public function delete($id)
    {
        $product = Product::find($id);
        if ($product){

            \Storage::delete($product->image);
            uploadFiles()->delete_files($product->id);

            $product->delete();

            return redirect()->back()->with(['success' => 'Done Delete Product']);
        }
        return redirect()->back()->with(['error' => 'Not found this product']);
    }

    public function deleteMultiImage($request)
    {
        $file = File::find($request->id);
        if ($file){
            \Storage::delete($file->full_file);
            $file->delete();

            return 'Image Deleted Successfully';
        }
        return 'Not found this image';
    }
}
