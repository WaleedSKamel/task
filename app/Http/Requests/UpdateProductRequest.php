<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:2|max:255',
            'description' => 'required|string|min:2',
            'price' =>'required|regex:/^\d+(\.\d{1,2})?$/|min:0',
            'image' => 'sometimes|nullable|image|mimes:jpg,jpeg,png,bmp'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'This filed name is required',
            'name.min' => 'This filed name must be min 2 char',
            'name.max' => 'This filed name must be max 255 char',
            'name.string' => 'This filed name must be char',

            'description.required' => 'This filed description is required',
            'description.min' => 'This filed description must be min 2 char',
            'description.string' => 'This filed description must be char',

            'price.required' => 'This filed price is required',
            'price.min' => 'This filed price must be min 0',
            'price.regex' => 'This filed price not match',

            'image.image' => 'This filed image must be image',
            'image.mimes' => 'This filed image is mimes jpg,jpeg,png,bmp',
        ];
    }
}
