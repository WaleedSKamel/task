<?php

namespace App\Http\Controllers\Upload;

use App\Http\Controllers\Controller;
use App\Models\File;

class UploadController extends Controller
{
    /*$request, $path, $upload_type = 'single', $delete_file = null , $newName = null, $crud_type =[]*/

    public function upload($data = []){

        if (\request()->hasFile($data['file']) && $data['upload_type'] == 'single'){
            \Storage::has($data['delete_file']) ? \Storage::delete($data['delete_file']) : '' ;
            return  \request()->file($data['file'])->store($data['path']);
        }
        elseif (\request()->hasFile($data['file']) && $data['upload_type'] == 'files' && $data['multi_upload'] == null){
            $file =  \request()->file($data['file']);
            $size = $file->getSize();
            $main_type = $file->getType();
            $name = $file->getClientOriginalName();
            $hash_name = $file->hashName();
            $full_file = $data['path'] .'/'. $hash_name;

            $file->store($data['path']);
            $add = File::create([
                'name'=>$name,
                'size'=>$size,
                'file'=>$hash_name,
                'path'=>$data['path'],
                'full_file'=>$full_file,
                'mime_type'=>$main_type,
                'file_type'=>$data['file_type'],
                'relation_id'=>$data['relation_id'],
            ]);

            return  $add->id;
        }
        elseif (\request()->hasFile($data['file']) && $data['upload_type'] == 'files' && $data['multi_upload'] == true){
            $files =  \request()->file($data['file']);
            $ids =[];
            foreach ($files as $key => $file){
                $size = $file->getSize();
                $main_type = $file->getType();
                $name = $file->getClientOriginalName();
                $hash_name = $file->hashName();
                $full_file = $data['path'] .'/'. $hash_name;

                $file->store($data['path']);
                $add = File::create([
                    'name'=>$name,
                    'size'=>$size,
                    'file'=>$hash_name,
                    'path'=>$data['path'],
                    'full_file'=>$full_file,
                    'mime_type'=>$main_type,
                    'file_type'=>$data['file_type'],
                    'relation_id'=>$data['relation_id'],
                ]);
                $ids[] = $add->id;
            }
            return $ids;

        }

    }

    public function delete($id)
    {
        $file = File::find($id);
        if (!empty($file)){
            \Storage::delete($file->full_file);
            $file->delete();
        }

    }

    public function delete_files($product_id)
    {
        $files = File::where('relation_id','=',$product_id)
            ->where('file_type','=','product')->get();
        if (count($files)  > 0 ){
            foreach ($files as $file){
                $this->delete($file->id);
                \Storage::deleteDirectory($file->path);
            }
        }


    }
}
