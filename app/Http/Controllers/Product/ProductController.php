<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Interfaces\ProductRepositoryInterface;
use Illuminate\Http\Request;
class ProductController extends Controller
{
    private $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }
    public function all()
    {
        $products = $this->productRepository->all();
        return view('backend.product.index',compact('products'));
    }

    public function create()
    {
        return view('backend.product.create');
    }

    public function store(StoreProductRequest $request)
    {
        return $this->productRepository->store($request);
    }

    public function edit($id)
    {
        return $this->productRepository->edit($id);

    }

    public function update(UpdateProductRequest $request,$id)
    {
        return $this->productRepository->update($request,$id);
    }

    public function delete($id)
    {
        return $this->productRepository->delete($id);
    }

    public function deleteMultiImage(Request $request)
    {
        return $this->productRepository->deleteMultiImage($request);
    }
}
