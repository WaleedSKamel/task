<?php

namespace App\Interfaces;

interface ProductRepositoryInterface
{
    public function all();

    public function store($request);

    public function edit($id);

    public function update($request, $id);

    public function delete($id);

    public function deleteMultiImage($request);
}
