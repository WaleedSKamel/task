<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('backend.home');
})->name('home');

Route::group(['prefix'=>'product','namespace'=>'Product'],function (){

    Route::get('all','ProductController@all')->name('product.view');
    Route::get('create','ProductController@create')->name('product.create');
    Route::post('store','ProductController@store')->name('product.store');
    Route::get('edit/{id}','ProductController@edit')->name('product.edit');
    Route::post('update/{id}','ProductController@update')->name('product.update');
    Route::get('delete/{id}','ProductController@delete')->name('product.delete');
    Route::post('delete/images','ProductController@deleteMultiImage')->name('image.delete');

    Route::view('products','backend.product.product',[
        'products' => \App\Models\Product::OrderByDesc('created_at')->get()
    ])->name('products');
});
