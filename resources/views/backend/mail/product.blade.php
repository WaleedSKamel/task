@component('mail::message')
    # Create new product
    This product name {{$data->name }}

    @component('mail::table')
        | Product Name| Price | Description
        | --------------- |:----------------:| ----------------:|
        |{{ $data->name }} | {{ $data->price }} | {{ $data->description }}|
    @endcomponent


@endcomponent
