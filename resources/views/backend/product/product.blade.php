
<table id="example1" class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>#</th>
        <th>Name</th>
        <th>Price</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($products as $key => $product)
        <tr>
            <td>{!! $key+1 !!}</td>
            <td>{!! $product->name !!}</td>
            <td>{!! $product->price !!}</td>
            <td class="project-actions">
                <a class="btn btn-info btn-sm" href="{!! route('product.edit',$product->id) !!}">
                    <i class="fas fa-pencil-alt">
                    </i>
                    Edit
                </a>
                <a class="btn btn-danger btn-sm" href="{!! route('product.delete',$product->id) !!}">
                    <i class="fas fa-trash">
                    </i>
                    Delete
                </a>
            </td>
        </tr>
    @endforeach

    </tbody>
    <tfoot>
    <tr>
        <th>#</th>
        <th>Name</th>
        <th>Price</th>
        <th>Action</th>

    </tr>
    </tfoot>
</table>

