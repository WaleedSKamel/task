@extends('backend.layout.master')

@section('title','View Product')

@push('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{!! asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') !!}">
@endpush
@section('content-header')

@endsection

@section('content')
    @include('backend.partials.message')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="card">
                    <div class="card-header ">
                        <h3 class="card-title pull-right">View Product</h3>

                        <a href="{!! route('product.create') !!}" class="btn btn-primary float-right">
                            <i class="fa fa-plus"></i> Create Product
                        </a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body" id="products">

                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
@endsection
@push('script')
    <script src="{!! asset('backend/plugins/datatables/jquery.dataTables.min.js') !!}"></script>
    <script src="{!! asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') !!}"></script>
    <script src="{!! asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js') !!}"></script>
    <script src="{!! asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') !!}"></script>

    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true,
                "autoWidth": false,
            });

        });

        var auto_refresh = setInterval(function (){
            $('#products').load('{!! route('products') !!}').fadeIn("slow")
        },100)
    </script>
@endpush
