@extends('backend.layout.master')

@section('title','Create Product')

@section('content-header')

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Create Product</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                        {!! Form::open(['url' => route('product.store'),'method' => 'post' ,'files' => true]) !!}
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Name</label>
                                <input type="text" name="name" value="{!! old('name') !!}" class="form-control @error('name') is-invalid @enderror" id="exampleInputEmail1" placeholder="Name">
                                @error('name')
                                <span class="text-danger">{!! $message !!} </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label>Description</label>
                                <textarea name="description" class="form-control @error('description') is-invalid @enderror" rows="3" placeholder="Description">{!! old('description') !!}</textarea>
                                @error('description')
                                <span class="text-danger">{!! $message !!} </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Price</label>
                                <input type="text" name="price" value="{!! old('price') !!}" class="form-control @error('price') is-invalid @enderror" id="exampleInputPassword1" placeholder="Price">

                                @error('price')
                                <span class="text-danger">{!! $message !!} </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <img src="{!! asset('image/default.png') !!}" class="profile-user-img img-responsive img-circle image-preview" alt="" style="height: 100px; width: 100px">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">IMG</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" name="image" class="custom-file-input image @error('image') is-invalid @enderror" id="exampleInputFile">
                                        <label class="custom-file-label" for="exampleInputFile">Choose image</label>
                                    </div>
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="">Upload</span>
                                    </div>

                                </div>
                                @error('image')
                                <span class="text-danger">{!! $message !!} </span>
                                @enderror
                            </div>



                            <div class="form-group">
                                <label for="exampleInputFile">Multi Images</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" name="images[]" multiple class="custom-file-input" id="exampleInputFile">
                                        <label class="custom-file-label" for="exampleInputFile">Choose images</label>
                                    </div>
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="">Upload</span>
                                    </div>
                                </div>
                            </div>
                        </div>





                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> Save</button>
                        </div>
                   {!! Form::close() !!}
                </div>
                <!-- /.card -->
            </div>

        </div>
        <!-- /.row -->
@endsection

@push('script')
    <script>
        $(".image").change(function() {
            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('.image-preview').attr('src', e.target.result);
                }

                reader.readAsDataURL(this.files[0]);
            }
        });

    </script>
@endpush
